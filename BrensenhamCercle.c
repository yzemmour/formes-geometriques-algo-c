void BrensenhamCercle(int r,int x_centre,int y_centre, void *mlx, void *win)
{
    /* r: le rayon du cercle, x_centre et y_centre : coordonnes du rayon r */
    int x;
    int y;
    int d;

    x = 0;
    y = r;
    d = 5 - 4 * r;
    while(x <= y)
    {
        mlx_pixel_put(mlx, win, x + x_centre, y + y_centre, 0xFF0000);
        mlx_pixel_put(mlx, win, -x + x_centre, -y + y_centre, 0xFF0000);
        mlx_pixel_put(mlx, win, x + x_centre, -y + y_centre, 0xFF0000);
        mlx_pixel_put(mlx, win, -x + x_centre, y + y_centre, 0xFF0000);

        mlx_pixel_put(mlx, win, y + x_centre, x + y_centre, 0xFF0000);
        mlx_pixel_put(mlx, win, -y + x_centre, -x + y_centre, 0xFF0000);
        mlx_pixel_put(mlx, win, y + x_centre, -x + y_centre, 0xFF0000);
        mlx_pixel_put(mlx, win, -y + x_centre, x + y_centre, 0xFF0000);
        d += 8 * x + 4;
        x++;
        if (d > 0)
        {
            y--;
            d-= 8 * y;
        }
    }
}