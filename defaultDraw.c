# include <mlx.h>
# define WIN_LENGTH      2000
# define WIN_WIDTH       1000

typedef struct  s_mlx_environment
{
        void                    *mlx;
        void                    *win;
}                               t_env;
int var = 1;
int mouse_press(int button, int x, int y, void *param)
{
    param = 0;
    x = 1;
    y = 1;
    if(button == 1)
        var = 0;
        return (0);
}
int mouse_release(int button, int x, int y, void *param)
{
    param = 0;
    x = 1;
    y = 1;
    if(button == 1)
        var = 1;
        return (0);
}
int mouse_move(int x, int y, void *param)
{
    t_env *env;
    int i;
    int j;

    env = param;
    i = -4;
    j = -4;
    if (var == 0)
    {
        while(i <= 4 && j <= 4)
        {
            mlx_pixel_put(env->mlx, env->win,x+i,y+j,0xFF0000);
            i++;
            j++;
        }
    }
    return (0);
}
int main(void)
{
    t_env    env;
    env.mlx = mlx_init();
    env.win = mlx_new_window(env.mlx,WIN_LENGTH, WIN_WIDTH, "FRACTOL");
    mlx_hook(env.win,4,0,mouse_press,&env);
    mlx_hook(env.win,5,0,mouse_release,&env);
    mlx_hook(env.win,6,0,mouse_move,&env);
    mlx_loop(env.mlx);
    mlx_clear_window(env.mlx, env.win);
    mlx_destroy_window(env.mlx, env.win);
    return(0);
}